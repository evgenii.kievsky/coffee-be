from rest_auth.registration.serializers import RegisterSerializer
from rest_framework import serializers
from users.models import User
from allauth.account import utils


class UserRegisterSerializer(RegisterSerializer):
    email = serializers.EmailField(required=True)
    password1 = serializers.CharField(write_only=True)
    name = serializers.CharField(required=False, allow_null=True)
    role = serializers.IntegerField(required=False)

    def save(self, request):
        validated_data = self.get_cleaned_data()
        user = User.objects.create(
            email=validated_data['email'],
            name=validated_data['name'],
            role=validated_data['role'],
            password=validated_data['password1']
        )
        self.custom_signup(request, user)
        utils.setup_user_email(request, user, [])
        return user

    def get_cleaned_data(self):
        data = super(UserRegisterSerializer, self).get_cleaned_data()
        additional_data = {
            'name': self.validated_data.get('name', ''),
            'role': self.validated_data.get('role', ''),
        }
        data.update(additional_data)
        return data

    class Meta:
        model = User


class UserDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'name', 'role')
        read_only_fields = ('email',)


