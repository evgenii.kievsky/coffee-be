from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'register', views.UserCreateView.as_view()),
    # url(r'users/list', views.UserViewSet.as_view({'get': 'list'})),
]