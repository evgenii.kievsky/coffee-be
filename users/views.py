from users.models import User
from users.serializers import UserDetailsSerializer, UserRegisterSerializer
from rest_framework import viewsets
from rest_auth.registration.views import RegisterView


class UserCreateView(RegisterView):
    serializer_class = UserRegisterSerializer
    queryset = User.objects.all()

    class Meta:
        model = User
        fields = ('email', 'date_of_birth', 'role', 'name')


class UserViewSet(viewsets.ModelViewSet):
    queryset = (
        User.objects
        .all()
    )
    serializer_class = UserDetailsSerializer


