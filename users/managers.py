from django.contrib.auth.models import BaseUserManager, UserManager
from . import models
import datetime


class UserManager(BaseUserManager):

    use_in_migrations = True

    def create_user(self, email, password=None, **extra_fields):
        user = self.model(
            email=self.normalize_email(email),
            name=extra_fields["name"],
            role=extra_fields["role"],
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, password):
        user = self.create_user(
            email,
            password=password,
            role=models.UserRole.ADMIN.value,
            name=name,
        )
        user.is_admin = True
        return user

