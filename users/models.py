from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractBaseUser
from users import managers
from common import datatypes as common_datatypes


class UserRole(common_datatypes.ChoiceEnum):

    ADMIN = 0
    CUSTOMER = 1
    SELLER = 2


class User(AbstractBaseUser):

    objects = managers.UserManager()
    username = None
    email = models.EmailField('email address', unique=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    role = models.IntegerField(choices=UserRole.choices(), null=True)
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

