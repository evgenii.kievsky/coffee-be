from django.contrib import admin
from users.models import User


class CustomUserAdmin(admin.ModelAdmin):
    fields = ('email', 'name', 'role')


admin.site.register(User, CustomUserAdmin)
