from enum import Enum, unique


@unique
class ChoiceEnum(Enum):

    @classmethod
    def choices(cls):
        return tuple((x.value, x.name) for x in cls)

